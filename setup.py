#!/usr/bin/env python

"""The setup script."""

from setuptools import setup, find_packages

with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()

requirements = ['Click>=7.0', ]

test_requirements = ['pytest>=3', ]

setup(
    author="Eamonn McCudden",
    author_email='emccudden@gmail.com',
    python_requires='>=3.6',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],
    description="distributed calculator for the dapr framework for python",
    entry_points={
        'console_scripts': [
            'dapr_python=dapr_python.cli:main',
        ],
    },
    install_requires=requirements,
    license="MIT license",
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    keywords='dapr_python',
    name='dapr_python',
    packages=find_packages(include=['dapr_python', 'dapr_python.*']),
    test_suite='tests',
    tests_require=test_requirements,
    url='https://github.com/emctl/dapr_python',
    version='0.1.0',
    zip_safe=False,
)

"""Top-level package for dapr-python."""

__author__ = """Eamonn McCudden"""
__email__ = 'emccudden@gmail.com'
__version__ = '0.1.0'
